const assert = require('assert');

describe('#test', function(){

    describe('#indexOf()', function(){
        it('debería retornar -1 cuando el valor no está en el array', function(){
            assert.strictEqual([1,2,3].indexOf(5),-1);
        })
    });

    describe('Cálculos aritméticos', ()=>{
        before(() =>{
            //Inicializar base de datos, variables...
            console.log('Este codigo se ejecuta antes de cualquier tester');
        });

        after(()=>{
            //Limpiar base de datos, o tablas, arrays o variables
            console.log('Este código se ejecuta después de todos los test');
        });

        //Describe permite agrupar pruebas similares en el mismo bloquear
        describe('En este test se prueba una suma',()=>{
            it('debería retornar 5',()=>{
                let n1 = 2;
                let n2 = 3;
                assert.strictEqual(n1+n2,5);
            });
        });
        describe('En este test se prueba una resta',()=>{
            it('debería retornar 2',()=>{
                let n1 = 5;
                let n2 = 3;
                assert.strictEqual(n1-n2,2);
            });
        });
        describe('En este test se prueba una multiplicación',()=>{
            it('debería retornar 10',()=>{
                let n1 = 5;
                let n2 = 2;
                assert.strictEqual(n1*n2,10);
            });
        });
        describe('En este test se prueba una división',()=>{
            it('debería retornar 2',()=>{
                let n1 = 10;
                let n2 = 5;
                assert.strictEqual(n1/n2,2);
            });
        });

    });
})