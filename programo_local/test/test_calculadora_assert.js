const assert = require('chai').assert;
const calculadora = require('../app/calculadora');

describe('Test de calculadora usando ASSERT interface de chai ', ()=>{
    describe('Verificar función sumar', () => {

        it('Debería retornar un valor igual a 3', ()=>{
            const result = calculadora.sumar(1,2);
            assert.equal(result, 3);
        });

        it('Debería ser de tipo número', ()=>{
            const result = calculadora.sumar(1,2);
            console.log('tipo de valor es:', typeof(result));
            assert.typeOf(result, 'number');
        });

    });
});