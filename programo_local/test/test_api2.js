const assert = require('assert');
const fetch = require('node-fetch');

describe('Probando API 2',()=>{

    it('API responde 200', async()=>{
        await fetch('https://api.openweathermap.org/data/2.5/weather?q=ushuaia&appid=e234528b4d2b63e0641435b56c72e378')
        .then(response =>{
            console.log(response.status)
            assert.strictEqual(response.status, 200);
        })
    });

    it('API responde 401', async()=>{
        await fetch('https://api.openweathermap.org/data/2.5/weather?q=ushuaia&appid=e234528b4d2543b63e0641435b56c72e378')
        .then(response =>{
            console.log(response.status)
            assert.strictEqual(response.status, 401);
        })
    });

    });